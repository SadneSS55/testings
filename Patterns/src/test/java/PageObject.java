
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PageObject {
    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./resources/GoogleDriver/chromedriver.exe");
        driver = new ChromeDriver();
    }

    public PageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    public void open(){
        driver.get("https://playground.learnqa.ru/puzzle/triangle");
    }

    @FindBy(id = "show_answ")
    private WebElement show;
    @FindBy(linkText = "Ссылка на ответы")
    private WebElement answer;
    @FindBy(id = "hide_answ")
    private WebElement hide;

    public boolean isShowAnswerButtonExist(){
        return show.isDisplayed();
    }

    public boolean isLinkForAnswerExist(){
        showAnswer.click();
        wait.until(ExpectedConditions.elementToBeClickable(answer));
        return answer.isDisplayed();
    }

    public boolean isHideAnswerButtonExist(){
        return hide.isDisplayed();
    }

}
