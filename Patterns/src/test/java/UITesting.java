

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class UITesting {
    WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./resources/GoogleDriver/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void pageTest() {
        PageObject pageObject = PageFactory.initElements(driver, PageObject.class);
        pageObject.open();

        assertTrue(pageObject.isShowAnswerButtonExist());
        assertTrue(pageObject.isLinkForAnswerExist());
        assertTrue(pageObject.isHideAnswerButtonExist());
    }


}
