import org.assertj.core.api.AbstractAssert;
import org.json.JSONObject;

public class StringMatcher extends AbstractAssert<StringMatcher, String> {

    protected StringMatcher(String s) {
        super(s, StringMatcher.class);
    }

    public StringMatcher containsSuccessAndStatus() {
        JSONObject obj = new JSONObject(this.actual);
        if(!obj.getString("status").equals("success")) {
            failWithMessage("StringMatcher error: no status or success");
        }
        return this;
    }

}